<img style="float: right" align="right" src="https://gitlab.com/pastel-fablab/orga/raw/master/logo/fablab-logo-transparent-300dpi.png" width="350">

# Projet rpi-object-tracking

Ce projet est un exemple de mise en place d'un environnemnt complet sur raspberry pi (rpi4) pour réaliser du suivi d'object(object tracking) en utilisant la librairie graphique libre OpenCV (https://opencv.org).
Au niveau matériel, un kit pan-tilt a été utilisé (rpi hat avec capteur de luminosité et permettant de commander 2 servos moteurs).
La caméra utilisée est une caméra raspberry reliée au rpi via une nappe.

[mettre un schéma d'architecture générale illutrant le projet. Au pire, une photo]

## Fiche projet 
| **Caractéristiques du Projet**  |   |
| --- | --- |
| **Technologies** | raspberry pi, camera, servos, pan-tilt, opencv, python
| **Coût** | Hors coût du raspberry et son alimentation: pan-tilt hat - 20 euros, camera rpi - 28 euros
| **Temps de réalisation** | 30 minutes de montage du kit et branchement, 30 minutes pour mettre en place l'environnement 
| **Difficulté** | Faible
| **Equipe réalisatrice** | Benoit Martineau
| **Documentation** | https://gitlab.com/pastel-fablab/projets/rpi-object-tracking

## Matériel

- rpi4 et son alimentation ad-hoc
- camera rpi avec sa nappe de connexion
- un kit pan-tilt complet (c.à.d rpi hat comprenant le chassis, 2 servos)

## Description détaillée 

Ce projet est un exemple de mise en place d'un environnement complet sur raspberry pi (rpi4) pour réaliser du suivi d'object(object tracking) en utilisant la librairie graphique libre OpenCV (https://opencv.org).
Au niveau matériel, un kit pan-tilt a été utilisé (rpi hat avec capteur de luminosité et permettant de commander 2 servos moteurs).
La caméra utilisée est une caméra raspberry reliée au rpi via une nappe.

En suivant les directives du fichier howto.txt, vous pourrez:
- activer / tester une caméra rpi reliée par nappe à la framboise,
- mettre en place un environnement de développement python/opencv,
- télécharger et "installer" les drivers d'un HAT de marque WaveShare,
- télécharger et exécuter les programmes d'exemples du HAT permettant d'en vérifier le bon fonctionnement,
- lancer un simple python permettant de remettre en position initiale les servos(servos_reset.py),
- lancer un simple python permettant de tester une capture vidéo via opencv(test_capture.py),
- lancer un exemple de script python qui détecte et suit un objet de forme ronde et de couleur rouge(red_ball.py).

## Liens

- Kubii: https://www.kubii.fr
- Waveshare: https://www.waveshare.com/pan-tilt-hat.htm
- Pimoroni: https://shop.pimoroni.com/products/pan-tilt-hat
- OpenCV: https://opencv.org

## License

Ce projet est sous licence Apache 2.0. Voire le fichier [LICENSE](LICENSE.md) pour plus de détails
