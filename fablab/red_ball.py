import cv2
import numpy as np
from PCA9685 import PCA9685

pwm = PCA9685(0x40, debug=False)
pwm.setPWMFreq(50)

#Rotation Tete - Axe Y
pwm.setRotationAngle(0, 90)
#Rotation Tourelle - Axe X
pwm.setRotationAngle(1, 90)

cap = cv2.VideoCapture(0)

# Set camera resolution
cap.set(3, 480)
cap.set(4, 320)

_, frame = cap.read()
rows, cols, _ = frame.shape

x_medium = int(cols / 2)
x_center = int(cols / 2)
y_medium = int(cols / 2)
y_center = int(cols / 2)
x_position = 90 # degrees
y_position = 90 # degrees

while True:
    _, frame = cap.read()
    hsv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
    # red color
    low_red = np.array([161, 155, 84])
    high_red = np.array([179, 255, 255])
    red_mask = cv2.inRange(hsv_frame, low_red, high_red)
    contours, _ = cv2.findContours(red_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=lambda x:cv2.contourArea(x), reverse=True)
    
    for cnt in contours:
        (x, y, w, h) = cv2.boundingRect(cnt)
        
        x_medium = int((x + x + w) / 2)
        y_medium = int((y + y + w) / 2)
        break
    
    cv2.line(frame, (x_medium, 0), (x_medium, 320), (0, 255, 0), 2)
    cv2.line(frame, (0, y_medium), (480, y_medium), (0, 255, 0), 2)

    # Move servo motor on X
    if ( x_medium < x_center - 30 ) and ( x_position < 170 ):
        x_position += 1
    elif ( x_medium > x_center + 30 ) and ( x_position > 10 ):
        x_position -= 1
    pwm.setRotationAngle(1, x_position)

    # Move servo motor on Y
    if ( y_medium < y_center - 30 ) and ( y_position > 45 ):
        y_position -= 1
    elif ( y_medium > y_center + 30 ) and ( y_position < 100 ):
        y_position += 1
    pwm.setRotationAngle(0, y_position)

    cv2.line(frame, (x_medium, 0), (x_medium, 480), (0, 255, 0), 2)
    cv2.line(frame, (0, y_medium), (480, y_medium), (0, 255, 0), 2)
    
    cv2.putText(frame, "x_medium: "+str(x_medium), (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
    cv2.putText(frame, "x_center: "+str(x_center), (10, 80), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
    cv2.putText(frame, "x_position: "+str(x_position), (10, 110), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
    cv2.putText(frame, "y_medium: "+str(y_medium), (10, 140), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
    cv2.putText(frame, "y_center: "+str(y_center), (10, 170), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
    cv2.putText(frame, "y_position: "+str(y_position), (10, 200), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
 
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1)
    
    if key == 27:
        break
    
cap.release()
cv2.destroyAllWindows()
