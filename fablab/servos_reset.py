#import cv2
#import numpy as np
from PCA9685 import PCA9685

pwm = PCA9685(0x40, debug=False)
pwm.setPWMFreq(50)

#Rotation Tete - Axe Y
pwm.setRotationAngle(0, 90)
#Rotation Tourelle - Axe X
pwm.setRotationAngle(1, 90)

